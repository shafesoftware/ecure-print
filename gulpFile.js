var gulp = require('gulp');
var scss = require('gulp-sass');
var jade = require('gulp-jade');

gulp.task('scss', function () {
    return gulp.src('./template/scss/style.scss')
        .pipe(scss().on('error', scss.logError))
        .pipe(gulp.dest('./build/css'));
});


gulp.task('html', function() {
    var YOUR_LOCALS = {};

    gulp.src(['./template/jade/*.jade'])
        .pipe(jade({
            locals: YOUR_LOCALS, pretty: true
        }))
        .pipe(gulp.dest('./build/html'))
});



gulp.task('default', ['scss', 'html']);